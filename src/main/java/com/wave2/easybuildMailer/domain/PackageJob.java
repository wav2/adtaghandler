package com.wave2.easybuildMailer.domain;

import com.wave2.easybuildMailer.util.EMProperties;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Richard on 06/07/2016.
 */
public class PackageJob {
    Logger log = Logger.getLogger(PackageJob.class);
    EMProperties eMProperties = EMProperties.getInstance();
    List<Job> jobs = new ArrayList<Job>();

    public void addJob(Job job) {
        jobs.add(job);
    }

    public List<Job> getJobs() {
        return jobs;
    }

    public void checkOtherJobs() throws IOException {
        Job job = this.jobs.get(0);
        if (job != null) {
            File filePath = new File(job.getFilePath());
            String[] sizes = eMProperties.getPropertyDefault("mailer.dimensions.template.sizes","").split(",");
            if (sizes.length == 0)
                log.warn("mailer.dimensions.template.sizes doesn't contain any sizes, e.g mpu,sk,mb");

            String triggerSize = StringUtils.substringBetween(filePath.getName(), "_", "_");
            if (triggerSize == null)
                triggerSize = "";
            for (String size : sizes) {
                String file = filePath.getParent() + File.separatorChar + filePath.getName().replaceAll("(?i)_"+triggerSize, "_" + size);
                log.info("look for: " + file);
                if (new File(file).exists()) {
                    Job newJob = new Job(file);
                    if (! this.jobs.contains(newJob)) {
                        newJob.calculateJobDimentions();
                        newJob.calculateTagUrl();
                        this.jobs.add(newJob);
                    }
                }
            }
        }
    }
}
