package com.wave2.easybuildMailer.domain;

/**
 * Created by Richard on 22/07/2016.
 */
public enum Format {
    SK("Skyscraper"),
    SKYSCRAPER("Skyscraper"),
    LB("Leaderboard"),
    LEADERBOARD("Leaderboard"),
    MPU("Mid Page Unit"),
    DMPU("Double Mid Page Unit"),
    MB("Mobile"),
    MOBILE("Mobile"),
    TB("Tablet"),
    TABLET("Tablet"),
    BB("Billboard");

    String description = "";

    Format(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public static Format getByDescription(String description) {
        for(Format f : Format.values()) {
            if(f.getDescription() == description) return f;
        }
        return null;
    }
}
