package com.wave2.easybuildMailer.domain;

import com.wave2.easybuildMailer.util.EMProperties;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

/**
 * Created by Richard on 28/06/2016.
 */
public class Job {
    private Logger log = Logger.getLogger(Job.class);
    private EMProperties eMProperties = EMProperties.getInstance();
    private String jobName = "";
	private String jobId = "";
    private String filePath = "";
	private String templateName  = "";
    private String tagUrl = "";
    private String format = "";
    private Double width = 0.0;
    private Double height = 0.0;

    public Job() {}

    public Job(String filePath) throws IllegalArgumentException, IOException {
        this(filePath, new Double(0.0), new Double(0.0));
    }

    public Job(String filePath, Double width, Double height) throws IllegalArgumentException, IOException {
        setFilePath(filePath);
        if (width == 0.0 && height == 0.0) {
            calculateJobDimentions();
        }else {
            this.width = width;
            this.height = height;
        }
    }

    public void setFilePath(String filePath) throws IllegalArgumentException, IOException {
        File file = new File(filePath);
        if (!file.exists())
            throw new IllegalArgumentException("File path invalid: File does not exist");

        //Calculate Given Job Name
        int toFormat = file.getName().indexOf("_");
        int end = file.getName().indexOf("_", toFormat-1);
        if (end == -1) end = file.getName().indexOf(".");
        this.jobName = file.getName().substring(0,end);

        //Calculate Job Id (Name without Size)
        end = this.jobName.indexOf("_");
        if (end == -1) end = this.jobName.length();
        this.jobId = this.jobName.substring(0,end);

        this.filePath = file.getAbsolutePath();
        calculateTemplateName();
        this.width = 0.0;
        this.height = 0.0;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobName() {
        return this.jobName;
    }

    public String getJobId()
    {
        return this.jobId;
    }

    public String getFilePath() throws IOException {
        if (this.filePath.isEmpty())
            throw new IOException("FilePath not set");

        return this.filePath;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public double getWidth() {
        return this.width;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public double getHeight() {
        return this.height;
    }

    public void calculateTemplateName() throws IOException {
        if (filePath.isEmpty())
            throw new IOException("file path is empty");

        ZipFile zipFile;
        try {
            zipFile = new ZipFile(filePath);

            Enumeration<? extends ZipEntry> entries = zipFile.entries();

            while(entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                if (entry.getName().toLowerCase().endsWith(".html")) {
                    templateName = entry.getName();
                    break;
                }
            }
        }catch (ZipException e) {
            //Assume INDD or SWF where name is same unless JobXML id attribute changed it
            templateName = new File(filePath).getName();
        }
    }

    public String getTemplateName() {
        return this.templateName;
    }

    public String calculateTagUrl() {
        //String template = FilenameUtils.getBaseName(templateName);
        //tagUrl = new File(filePath).getName().replaceAll("(?i)"+format,template);
        String name = new File(filePath).getName();
        int end = name.indexOf("_");
        if (end == -1)
            end = name.indexOf(".");
        tagUrl = name.substring(0,end)+".html";
        return tagUrl;
    }

    public String getTagUrl() {
        return tagUrl;
    }

    public void calculateJobDimentions() {
        //Will be passed from EB rather than calculated at some point
        Boolean getFromTemplate = Boolean.valueOf(eMProperties.getPropertyDefault("mailer.dimensions.method.templateName","false"));
        if (getFromTemplate) {
            String[] sizes = eMProperties.getPropertyDefault("mailer.dimensions.template.sizes","").split(",");
            if (sizes.length == 0)
                log.warn("mailer.dimensions.template.sizes doesn't contain any sizes, e.g mpu,sk,mb");
            for (String size : sizes) {
                String match = new String("-" + size + ".html").toLowerCase();
                if (templateName.toLowerCase().endsWith(match)) {
                    setFormat(size);
                    String dimensions = eMProperties.getPropertyDefault("mailer.dimensions.template." + size, "");
                    if (!dimensions.isEmpty()) {
                        this.width = Double.parseDouble(dimensions.split("x")[0]);
                        this.height = Double.parseDouble(dimensions.split("x")[1]);
                    }else{
                        log.warn("mailer.dimensions.template."+size+" is missing or empty e.g 300x310");
                    }
                    break;
                }
            }
        }
    }

    @Override
    public boolean equals (Object aThat) {
        if ( !(aThat instanceof Job) ) return false;

        Job job = (Job)aThat;
        String path = "";
        try {
            path = job.getFilePath();
        }catch(IOException e) {
            //do nothing;
        }

        if (path.toLowerCase().equals(this.filePath.toLowerCase())) return true;
        return false;
    }

    public String getFormatDescription() {
        try {
            Format adFormat = Format.valueOf(format.toUpperCase());
            return adFormat.getDescription();
        }catch(IllegalArgumentException e) {
            log.error("No Ad Format found for " + format);
            return "";
        }
    }

    public void setFormat(String format) {
        this.format = format;
    }

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}


	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

}
