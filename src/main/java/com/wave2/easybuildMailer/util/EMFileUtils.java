package com.wave2.easybuildMailer.util;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created by Leigh on 02/08/2016.
 */
public class EMFileUtils {

	public void copyURLtoFile(URL url, File file) throws IOException {
		FileUtils.copyURLToFile(url,file);
	}
}
