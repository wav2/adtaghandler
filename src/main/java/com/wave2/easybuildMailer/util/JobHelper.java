package com.wave2.easybuildMailer.util;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Leigh on 29/07/2016.
 */
public class JobHelper {
    static Logger log = Logger.getLogger(JobHelper.class);
	EMProperties eMProperties = EMProperties.getInstance();

	private String tempFolder;

	private EMFileUtils fileUtils;

	public JobHelper() {
		this.tempFolder = eMProperties.getPropertyDefault("mailer.download.temppath","C:\\Mailer\\temp");
		this.fileUtils = new EMFileUtils();
	}

	public JobHelper(EMFileUtils fileUtils) {
		super();
		this.fileUtils = fileUtils;
	}

	public List<String> calculatePaths(String source) throws IOException {
		List<String> paths = new ArrayList<String>();
		JSONObject obj = new JSONObject(source);
		final JSONArray jobs = obj.getJSONArray("jobs");
		String id = UUID.randomUUID().toString();

        log.info("Number of jobs found in file : " + jobs.length());

		for (int jobIndex = 0; jobIndex < jobs.length(); jobIndex++) {
			JSONObject job = jobs.getJSONObject(jobIndex);
			final String jobToken = job.getString("jobToken");
			String[] jobParts = jobToken.split("/");
			String jobId = jobParts[1];
			String jobName = jobId;
//			if (job.has("jobName"))
//				jobName = job.getString("jobName");
//			else
//				jobName = jobId;
			String client = jobParts[0];
			final JSONArray outputs = job.getJSONArray("output");
			for (int outputIndex = 0; outputIndex < outputs.length(); outputIndex++) {
				JSONObject output = outputs.getJSONObject(outputIndex);
				final String type = output.getString("type");
				if (type.equals("jobxml") || type.equals("html5")) {

					String filename = downloadToLocal(client, jobName, jobId, id, jobIndex, type);
					if (type.equals("html5"))
						paths.add(filename);
				}
			}
		}
		return paths;
	}

	private String downloadToLocal(String client, String jobName, String jobId, String id, int jobIndex, String type) throws IOException {
		String baseUrl = eMProperties.getPropertyDefault("mailer.wave2pp.host.baseurl","http://localhost:8080/File");

		String fullUrl = baseUrl + "?command=document&client="+client+"&id="+jobId+"&type="+type+"&page=0";
		String extension = "";
		if (type.equals("html5"))
			extension = ".zip";
		else if (type.equals("jobxml"))
			extension = ".xml";
		String fileName = tempFolder + "/" + jobId + "_" + jobIndex + extension;
		fileUtils.copyURLtoFile(new URL(fullUrl), new File(fileName));
		return fileName;
	}
}
