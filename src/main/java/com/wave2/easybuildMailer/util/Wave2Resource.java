package com.wave2.easybuildMailer.util;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLDecoder;
//


/**
 * Utility class to allow indirect reference to resources stored in the Wave2 
 * directory structure.  Uses the 'Wave2Root' system property, if defined.
 * If the file is not found in the Wave2Root directory, attempt to load it using
 * the class loader mechanism, first the system, then the local class loader.
 * @author jgallimore@wav2.com
 * @author shodges@wav2.com
 */
public class Wave2Resource {
	private static final Logger log = Logger.getLogger(Wave2Resource.class);
	private String filename;
	
	public Wave2Resource() {
		super();
	}
	
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getDescription() {
		return "Wave2Resource: " + filename;
	}

	public InputStream getInputStream() throws IOException {
		return new FileInputStream(getFile());
	}

	public File getFile() throws IOException {
		File file = new File(getWave2Root() + File.separator + filename);
		if (!file.exists()) {
			URL url = ClassLoader.getSystemResource(filename);
			if (url == null) {
				url = getClass().getClassLoader().getResource(filename);
			}
			if (url != null) {
				file = new File(URLDecoder.decode(url.getFile(),"UTF-8"));
			}
			
			if (file == null || !file.exists()) {
				//file = super.getFile();
			}
		}
		
		log.info("Reading resource ["+file.getAbsoluteFile()+"]");
		
		return file;
	}

	public URL getURL() throws IOException {
		return getFile().toURL();
	}
	
	protected static String getWave2Root() {
		String result;
				
		String envRoot = System.getProperty("Wave2Root");
		if (envRoot != null && envRoot.length() > 0)
			result = envRoot;
		else
			result = calculateDrive();
		
		return result;
	}
	
	protected static String calculateDrive() {
		File[] files = File.listRoots();

		for (File file : files) {
			String path = file.getAbsolutePath();
			if (path.startsWith("a") || path.startsWith("A"))
				continue;
			file = new File(file, "Wave2");
			if (file.exists()) {
				log.info("Using " + file);
				return file.getAbsolutePath();
			}
		}
		log.info("Using C: as default");

		return "C:\\";
	}
}
