package com.wave2.easybuildMailer.util;


import com.wave2.easybuildMailer.AppMain;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class EMProperties {
	private static EMProperties _instance;
	private static final String PROPERTIES_FILENAME = "easybuildMailer.properties";
	private static Properties properties = new Properties();
	private static final Logger log = Logger.getLogger(EMProperties.class);

	private static String jarName = "easybuildMailer";

	/**
	 * As we don't have the Context path available. This method uses reflection
	 * to figure out where we are to know
	 *
	 * @return
	 */
	public static String getJarName(){
		//Random Class IN jar to get path
		String path = AppMain.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		if (path.toLowerCase().endsWith(".jar")) {
			String jarName = new File(path).getName();
			jarName = jarName.replaceAll(".jar", "");
			log.info("Get Jar Name by reflectpath: " + jarName);
		}else{
			log.info("Use default Jar Name: " + jarName);
		}

		return jarName;
	}

	private EMProperties() throws IOException {
		log.info("EDT Properties constructor loading default properties");
		InputStream defaultProperties = this.getClass().getResourceAsStream("/" + PROPERTIES_FILENAME);
		properties.load(defaultProperties);
		defaultProperties.close();
		//log.info("Listing default properties");
		//properties.list(System.out);

		char sep = File.separatorChar;
		String specificPropertiesFile = "config" + sep  + jarName + sep + PROPERTIES_FILENAME;
		log.info("Specific Properties File:" + specificPropertiesFile);

		//
		// Use the resource to load the properties file
		// 
		InputStream fis = null;
		try {
			Wave2Resource wave2Resource = new Wave2Resource();
			wave2Resource.setFilename(specificPropertiesFile);
			fis = wave2Resource.getInputStream();
			properties.load(fis);
			fis.close();
		}catch(IOException e){
			log.error("File loading exception getting resource.",e);
		}finally{
			if (fis != null) fis.close();
		}
/*		log.info("Listing properties");
		properties.list(System.out);*/
	}
	
	public static EMProperties getInstance() {
		try {
			//Set Jar Name for later
			jarName = getJarName();
			if (_instance == null) {
				synchronized (EMProperties.class) {
					if (_instance == null) {
						log.info("Setting instance of Order Manager properties");
						_instance = new EMProperties();
					}
				}
			}

			return _instance;
		} catch (IOException e){
			log.error("Error getting instance of EMProperties " +e.getLocalizedMessage());
			return null;
		}
	}
	
	public String getProperty(String propertyName) {
		if (properties.containsKey(propertyName)) {
			return properties.getProperty(propertyName);
		}else{
			log.error(propertyName+" does not exist in properties file "+PROPERTIES_FILENAME);
			return null;
		}
	}
	
	public String getPropertyDefault(String propertyName, String defaultValue) {
		if (properties.containsKey(propertyName)) {
			return properties.getProperty(propertyName);
		}else{
			log.error(propertyName+" does not exist in properties file "+PROPERTIES_FILENAME);
			return defaultValue;
		}
	}
	public void setProperty(String propertyName, String value) {
		try {
			properties.setProperty(propertyName, value);
		} catch (Exception e){
			
		}			
	}
}
