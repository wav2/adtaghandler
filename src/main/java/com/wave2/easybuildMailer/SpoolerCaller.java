package com.wave2.easybuildMailer;

import com.wave2.easybuildMailer.domain.Job;
import com.wave2.easybuildMailer.domain.PackageJob;
import com.wave2.easybuildMailer.email.Email;
import com.wave2.easybuildMailer.util.EMProperties;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.List;

/**
 * Created by Richard on 25/07/2016.
 */
public class SpoolerCaller {
    Logger log = Logger.getLogger(SpoolerCaller.class);
    private EMProperties eMProperties = EMProperties.getInstance();
    String filePath = "";
    String destPath = "";

    public SpoolerCaller(String[] args) {
        filePath = args[0];
        destPath = args[1];
    }

    public void process() throws Exception {
        Job job = new Job(filePath);
        job.calculateTemplateName();
        job.calculateJobDimentions();
        job.calculateTagUrl();

        PackageJob packageJob = new PackageJob();
        packageJob.addJob(job);
        packageJob.checkOtherJobs();

        List<Job> jobs = packageJob.getJobs();

        Email email = new Email();
		String body = email.buildEmailBody(jobs);
        email.sendEmail(body, jobs.get(0).getJobId());

        File destination = new File(destPath);
        if (!destination.exists())
            destination.mkdirs();

        if (destination.isDirectory() && destination.canWrite()) {
            for (Job thisJob : jobs) {
                String srcPath = thisJob.getFilePath();
                File srcFile = new File(srcPath);
                String fullDestPath = destPath + File.separatorChar + srcFile.getName();
                File destFile = new File(fullDestPath);

                Boolean moveFiles = Boolean.valueOf(eMProperties.getPropertyDefault("mailer.move.processed.files","true"));
                if (moveFiles) {
                    log.info("Move "+srcPath+" to "+fullDestPath);
                    srcFile.renameTo(destFile);
                }else {
                    log.info("Copy "+srcPath+" to "+fullDestPath);
                    FileUtils.copyFile(srcFile, destFile);
                }
            }
        }else{
            log.error(destPath + " exists but is not a directory or can not be written to");
        }
    }
}
