package com.wave2.easybuildMailer.email;

import com.wave2.easybuildMailer.domain.Job;
import com.wave2.easybuildMailer.util.EMProperties;
import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ResourceNotFoundException;

import java.io.File;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;

/**
 * Created by Richard on 24/03/2016.
 * Wave2 Media Solutions
 */
public class VelocityEmail {
    static private Logger log = Logger.getLogger("VelocityEmail.class");
    static private EMProperties properties = EMProperties.getInstance();
    private String velocityTemplatePath;

    public String buildMessage(String velocityTemplatePath, List<Job> jobs) {
        try {
            Properties p = new Properties();
            p.setProperty("file.resource.loader.path", new File(velocityTemplatePath).getParent());
            p.setProperty("input.encoding", "UTF-8");
            p.setProperty("output.encoding", "UTF-8");

            VelocityEngine ve = new VelocityEngine();
            ve.init(p);

            VelocityContext context = new VelocityContext();
            context.put("dateTool", new org.apache.velocity.tools.generic.DateTool());
            context.put("numberTool", new org.apache.velocity.tools.generic.NumberTool());
            context.put("conversionTool", new org.apache.velocity.tools.generic.ConversionTool());
            context.put("esc", new org.apache.velocity.tools.generic.EscapeTool());
            context.put("math", new org.apache.velocity.tools.generic.MathTool());
            context.put("loop", new org.apache.velocity.tools.generic.LoopTool());

            context.put("jobs", jobs);

            StringWriter writer = new StringWriter();
            // codeException
            log.info("use template: " + velocityTemplatePath);
            ve.mergeTemplate(new File(velocityTemplatePath).getName(), "UTF-8", context, writer);
            String message = writer.toString();
            log.debug("getMessage: message:" + message);
            return message;

        } catch (ResourceNotFoundException e) {
            log.debug("Alert Email File velocity File does not exist default msg returned");
            return "Default message template does not exist or is incorrectly named.";
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.debug("Alert Email file velocity File does not exist default msg returned");
        return "Default message template does not exist or is incorrectly named.";
    }

}
