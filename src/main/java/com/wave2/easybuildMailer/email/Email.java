package com.wave2.easybuildMailer.email;

import com.wave2.easybuildMailer.domain.Job;
import com.wave2.easybuildMailer.util.EMProperties;
import org.apache.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 * Created by Richard on 24/03/2016.
 * Wave2 Media Solutions
 */
public class Email {
    private static Logger log = Logger.getLogger(Email.class);
    private static String EMAIL_BOOKING_TEMPLATE = "BookingTemplate.vm";
    private static EMProperties properties = EMProperties.getInstance();

    public Email() {
    }

    public String buildEmailBody(List<Job> jobs) throws Exception {
		String bookingTemplatePath = properties.getPropertyDefault("email.template.booking", getClass().getClassLoader().getResource(EMAIL_BOOKING_TEMPLATE).getPath());
		VelocityEmail velocityEmail = new VelocityEmail();
		String emailBody = velocityEmail.buildMessage(new File(bookingTemplatePath).getCanonicalPath(), jobs);
		return emailBody;
	}

	public int sendEmail(String emailBody, String jobId) throws Exception {

        String auth = properties.getPropertyDefault("email.smtp.auth", "false");
        Properties p = new Properties();
        p.put("mail.transport.protocol", properties.getProperty("email.transport.protocol"));
        p.put("mail.host", properties.getProperty("email.smtp.host"));
        p.put("mail.smtp.port", properties.getProperty("email.smtp.port"));
        p.put("mail.user", properties.getProperty("email.smtp.auth.user"));
        p.put("mail.password", properties.getProperty("email.smtp.auth.pwd"));
        p.put("mail.smtp.auth", auth);
        p.put("mail.smtp.starttls.enable", properties.getProperty("email.smtp.starttls.enable"));
        p.put("mail.mime.charset","utf-8");
        p.put("mail.smtp.socketFactory.class", properties.getPropertyDefault("email.smtp.socketfactory", "javax.net.ssl.SSLSocketFactory"));
        //MailSSLSocketFactory sf = new MailSSLSocketFactory();
        //sf.setTrustAllHosts(true);
        //p.put("mail.smtp.ssl.socketFactory", sf);
        p.put("mail.smtp.ssl.enable", properties.getProperty("email.smtp.ssl.enable"));


        SMTPAuthenticator authenticator = null;
        if (Boolean.valueOf(auth)) {
            authenticator = new SMTPAuthenticator();
        }
        Session session = Session.getInstance(p, authenticator);
        session.setDebug(Boolean.valueOf(properties.getPropertyDefault("email.smtp.debug","false")));

        String to = properties.getProperty("email.booking.to");
        String from = properties.getProperty("email.booking.from");
        String fromName = properties.getProperty("email.booking.fromName");
        String subject = properties.getProperty("email.booking.subject").replace("{jobid}", jobId);
        Message message = buildMimeMessage(session, to, from, fromName, subject, emailBody);

        Transport transport = session.getTransport("smtp");
        transport.connect();
        transport.send(message);
        transport.close();
		return 1;
    }

    private Message buildMimeMessage(Session session, String to, String from, String fromName, String subject, String body) throws MessagingException, UnsupportedEncodingException {
        Message message = new MimeMessage(session);
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
        message.setFrom(new InternetAddress(from, fromName));
        message.setSubject(subject);
        message.setSentDate(new Date());

        BodyPart messageBodyPart  = new MimeBodyPart();
        messageBodyPart.setContent(body, "text/html; charset=utf-8");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);

        message.setContent(multipart,"multipart/alternative");
        return message;
    }

    private class SMTPAuthenticator extends javax.mail.Authenticator {
        public PasswordAuthentication getPasswordAuthentication() {
            String username = properties.getPropertyDefault("email.smtp.auth.user", "");
            String password = properties.getPropertyDefault("email.smtp.auth.pwd", "");
            return new PasswordAuthentication(username, password);
        }
    }
}
