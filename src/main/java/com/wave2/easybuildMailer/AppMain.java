package com.wave2.easybuildMailer;

import com.wave2.easybuildMailer.email.Email;
import com.wave2.easybuildMailer.util.EMProperties;
import com.wave2.easybuildMailer.util.JobHelper;
import org.apache.log4j.Logger;

/**
 * Created by Richard on 27/06/2016.
 */
public class AppMain {
    static Logger log = Logger.getLogger(AppMain.class);
    static String data = "";
    EMProperties eMProperties = EMProperties.getInstance();
    static final String AD_TAG_MODE = "ADTAG";

    public AppMain(String[] args) throws Exception {
        try {
            log.info("AppMain : executing");
            data = args[0];
        }
        catch (ArrayIndexOutOfBoundsException e){
            log.info("Usage: <file path>");
            throw e;
        }

        log.info("Data input (Path/Json): "+data);
        String adTagHandlerMode = eMProperties.getProperty("mailer.mode");
        log.info("Ad Tag Handler Mode : " + adTagHandlerMode);

        if (args.length > 1) {
            log.info("Running in spooler mode");
            SpoolerCaller sp = new SpoolerCaller(args);
            sp.process();
        }
        else if (adTagHandlerMode.equals(AD_TAG_MODE)) {
            log.info("Running in Ad Tag Mode");
            EasyBuildCaller ec = new EasyBuildCaller(args[0], new JobHelper(), null);
            ec.processAdTag();
        }
        else {
            log.info("Running in Email Mode");
            EasyBuildCaller ec = new EasyBuildCaller(args[0], new JobHelper(), new Email());
            ec.processEmail();
        }
    }

    public static void main(String[] args) throws Exception {
        new AppMain(args);
    }
}
