package com.wave2.easybuildMailer;

import com.wave2.easybuildMailer.domain.Job;
import com.wave2.easybuildMailer.domain.PackageJob;
import com.wave2.easybuildMailer.email.Email;
import com.wave2.easybuildMailer.util.EMProperties;
import com.wave2.easybuildMailer.util.JobHelper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Richard on 25/07/2016.
 */
public class EasyBuildCaller {
    static Logger log = Logger.getLogger(AppMain.class);
    EMProperties eMProperties = EMProperties.getInstance();

	private String filename;
	private JobHelper jobHelper;
	private Email email;

	public EasyBuildCaller(String filename, JobHelper jobHelper, Email email) throws Exception {
		this.filename = filename;
		this.jobHelper = jobHelper;
		this.email = email;
	}

    public void processEmail () throws Exception {
        final String source = FileUtils.readFileToString(new File(filename));
        List<String> paths = jobHelper.calculatePaths(source);

        List<Job> jobList = new ArrayList<Job>();
        List<Job> packagedJobs = new ArrayList<Job>();
        for (String path : paths) {
            Job job = new Job(path);
            job.calculateTemplateName();
            job.calculateJobDimentions();
            job.calculateTagUrl();
            jobList.add(job);
            PackageJob packageJob = new PackageJob();
            packageJob.addJob(job);
            packageJob.checkOtherJobs();
            packagedJobs.addAll(packageJob.getJobs());
        }

        if (packagedJobs.size() > 0) {
            String body = email.buildEmailBody(packagedJobs);
            email.sendEmail(body, packagedJobs.get(0).getJobId());
        }
    }

    public void processAdTag() throws Exception {
		final String source = FileUtils.readFileToString(new File(filename));
		List<String> paths = jobHelper.calculatePaths(source);

		List<Job> jobList = new ArrayList<Job>();
		List<Job> packagedJobs = new ArrayList<Job>();

        List<String> adTags = new ArrayList<String>();

        String jsonOutput = "{\"adtags\":{";

		for (int pathIndex=0; pathIndex < paths.size() ;pathIndex++) {
            String path = paths.get(pathIndex);
			Job job = new Job(path);
			job.calculateTemplateName();
			job.calculateJobDimentions();
			job.calculateTagUrl();
			jobList.add(job);
			PackageJob packageJob = new PackageJob();

            String tagUrl = job.getTagUrl();
            tagUrl.replace(".html","-adtag.html");

            log.info("Tag Url : " + tagUrl);

            String jobName = job.getJobName().replace("_","-");
            String templateName = job.getTemplateName();
            double width = job.getWidth();
            double height = job.getHeight();

            String webserverBase = eMProperties.getProperty("webserver.base");

            String adTag = "<iframe src=\"" + webserverBase +
                    jobName + "/" + templateName +
                    "?trackurl=_ADCLICK_&ord=_ADTIME_\" " +
                    "width=\"" + width + "\" " +
                    "height=\"" + height + "\" " +
                    "scrolling=\"no\" frameborder=\"0\" " +
                    "></iframe>";

            //Escape xml so that it can be a property of JSON object
            String escapedXML = StringEscapeUtils.escapeJavaScript(adTag);

            //The node action handler is expecting a json obj
            //String jsonOutput = "{\"adtag\" : \""+ escapedXML +"\" }";
            jsonOutput += "\"" + job.getJobId() +  "\" : \""+ escapedXML +"\" ";

            if(pathIndex < ( (paths.size() - 1) )){
                jsonOutput += ",";
            }

            adTags.add(adTag);

            packageJob.addJob(job);
			packageJob.checkOtherJobs();
			packagedJobs.addAll(packageJob.getJobs());
		}

        jsonOutput += "}}";

        System.out.print(jsonOutput);
        log.info("Sending output : " + jsonOutput);

        log.info("AppMain - executing");
        log.info("Packed Jobs - : " + packagedJobs.size());

        log.info("All Jobs Processed");
    }
}
