package com.wave2.easybuildMailer;

import com.wave2.easybuildMailer.email.Email;
import com.wave2.easybuildMailer.util.EMProperties;
import com.wave2.easybuildMailer.util.JobHelper;
import junit.framework.TestCase;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Created by Richard on 27/06/2016.
 */
public class AppMainTest extends TestCase {

    @Test
    public void testMainNoArguments() throws Exception {
        String[] args = new String[0];
        try {
            AppMain.main(args);
        }catch (ArrayIndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }

    @Test
    public void testMainOneArgument() throws Exception {
        String[] args = new String[5];
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("W0011529.zip").getFile());

        args[0] = file.getAbsolutePath();
        args[1] = file.getParent()+File.separatorChar+"destination";
        AppMain.main(args);
    }

    @Test
    public void testMainMultiJob() throws Exception {
        String[] args = new String[5];
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("123456-0002_MPU_HTML5.zip").getFile());

        args[0] = file.getAbsolutePath();
        args[1] = file.getParent()+File.separatorChar+"destination";
        AppMain.main(args);
    }

	@Test
	public void testEasybuild() throws Exception {

		String[] args = new String[1];
		ClassLoader classLoader = getClass().getClassLoader();
//		File file = new File(classLoader.getResource("jsonJobInput.json").getFile());
		File file = new File(classLoader.getResource("2jsonJobInput.json").getFile());
		JobHelper jobHelper = mock(JobHelper.class);
		Email email = spy(new Email());
		List<String> testPaths = new ArrayList<String>();
		testPaths.add(classLoader.getResource("1469805651252_0.zip").getPath());
		testPaths.add(classLoader.getResource("1469805651250_1.zip").getPath());
		testPaths.add(classLoader.getResource("1469805651251_2.zip").getPath());
		when(jobHelper.calculatePaths(any(String.class))).thenReturn(testPaths);

		doReturn(0).when(email).sendEmail(any(String.class), any(String.class));
		final EasyBuildCaller easyBuildCaller = new EasyBuildCaller(file.getAbsolutePath(),jobHelper,email);
		easyBuildCaller.processEmail();
	}

	@Test
	public void testEasybuild2() throws Exception {

		String[] args = new String[1];
		ClassLoader classLoader = getClass().getClassLoader();
		EMProperties.getInstance().setProperty("email.template.booking",classLoader.getResource("BookingTemplate.vm").getFile());
//		File file = new File(classLoader.getResource("jsonJobInput.json").getFile());
		File file = new File(classLoader.getResource("tempJobInfo11672-13248-uvfsma").getFile());
		JobHelper jobHelper = mock(JobHelper.class);
		Email email = spy(new Email());
		List<String> testPaths = new ArrayList<String>();
		testPaths.add(classLoader.getResource("1470146725193_0.zip").getPath());
		testPaths.add(classLoader.getResource("1470146725194_1.zip").getPath());
		testPaths.add(classLoader.getResource("1470146725195_2.zip").getPath());
		when(jobHelper.calculatePaths(any(String.class))).thenReturn(testPaths);

		doReturn(0).when(email).sendEmail(any(String.class), any(String.class));
		final EasyBuildCaller easyBuildCaller = new EasyBuildCaller(file.getAbsolutePath(),jobHelper,email);
		easyBuildCaller.processEmail();
	}



}