package com.wave2.easybuildMailer.util;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.spy;

/**
 * Created by Leigh on 02/08/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class JobHelperTest {

	@Test
	public void testCalculatePaths() throws Exception {

		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("tempJobInfo11672-13248-uvfsma").getFile());
		final String json = FileUtils.readFileToString(file);

		EMFileUtils fileUtils = spy(new EMFileUtils());
		doAnswer(new Answer<Void>() {
			public Void answer(InvocationOnMock invocation) {
				Object[] args = invocation.getArguments();
				System.out.println("called with args: "+ Arrays.toString(args));
				return null;
			}
		}).when(fileUtils).copyURLtoFile(any(URL.class), any(File.class));
		JobHelper jb = new JobHelper(fileUtils);
		final List<String> paths = jb.calculatePaths(json);

		assertEquals(3,paths.size());
	}
}
