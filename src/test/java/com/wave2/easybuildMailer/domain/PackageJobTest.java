package com.wave2.easybuildMailer.domain;

import org.junit.Test;

import java.util.List;

import static junit.framework.Assert.assertEquals;

/**
 * Created by Richard on 19/07/2016.
 */
public class PackageJobTest  {

    @Test
    public void testCheckOtherJobs() throws Exception {
        Job job = new Job();
        String path = getClass().getClassLoader().getResource("123456-0002_MPU_HTML5.zip").getPath();
        job.setFilePath(path);
        job.calculateJobDimentions();

        PackageJob pkgJob = new PackageJob();
        pkgJob.addJob(job);
        pkgJob.checkOtherJobs();
        List<Job> jobs = pkgJob.getJobs();
        assertEquals(5,jobs.size());
    }
}