package com.wave2.easybuildMailer.domain;

import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by Richard on 08/07/2016.
 */
public class JobTest extends TestCase {

    @Test
    public void testCalculateTemplateNameException() throws Exception {
        try {
            Job job = new Job("W0011529.zip");
        }catch (IllegalArgumentException e) {
            assertTrue(true);
        }
    }

    @Test
    public void testCalculateTemplateNameINDD() throws Exception {
        Job job = new Job(getClass().getClassLoader().getResource("Elegant-0002-LB.indd").getPath());
        System.out.println("Template Name: "+job.getTemplateName());
        assertEquals("Elegant-0002-LB.indd", job.getTemplateName());
    }

    @Test
    public void testCalculateTemplateNameZip() throws Exception {
        Job job = new Job(getClass().getClassLoader().getResource("W0011529.zip").getPath());
        System.out.println("Template Name: "+job.getTemplateName());
        assertEquals("Hi-Tech-0009-MPU.html",job.getTemplateName());
    }
}