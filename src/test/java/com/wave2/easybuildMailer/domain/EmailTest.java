package com.wave2.easybuildMailer.domain;

import com.wave2.easybuildMailer.email.Email;
import com.wave2.easybuildMailer.util.EMProperties;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by Leigh on 01/08/2016.
 */
public class EmailTest {

	@Test
	public void testEmailText() throws Exception {

		ClassLoader classLoader = getClass().getClassLoader();

		Email email = new Email();
		List<Job> jobs = new ArrayList<Job>();
		Job job1 = new Job("C:\\Src\\easybuildMailer\\target\\test-classes\\1469805651252_0.zip",0.0,0.0);
		job1.setTemplateName("HTML5-0001-BB.html");
		job1.setJobId("jobId_12345");
		job1.setJobName("jobName_12345");
		Job job2 = new Job("C:\\Src\\easybuildMailer\\target\\test-classes\\1469805651250_1.zip",0.0,0.0);
		job2.setTemplateName("HTML5-0001-BB.html");
		job2.setJobName("jobName_12345");
		Job job3 = new Job("C:\\Src\\easybuildMailer\\target\\test-classes\\1469805651251_2.zip",0.0,0.0);
		job3.setTemplateName("HTML5-0001-BB.html");
		job3.setJobName("jobName_12345");
		jobs.add(job1);
		jobs.add(job2);
		jobs.add(job3);
		String body = email.buildEmailBody(jobs);

		StringWriter writer = new StringWriter();
		IOUtils.copy(classLoader.getResourceAsStream("testEmailBody.html"), writer, "UTF-8");
		final String testBody = writer.toString();

		assertEquals(removeWhiteSpaces(testBody),removeWhiteSpaces(body));
	}

	String removeWhiteSpaces(String input) {
		return input.replaceAll("\\s+", "");
	}

	@Test
	public void testNoSSLEmail() throws Exception {

		EMProperties em = EMProperties.getInstance();
		em.setProperty("email.smtp.ssl.enable", "false");
		em.setProperty("email.smtp.host", "10.0.0.78");
		em.setProperty("email.smtp.port", "25");
		em.setProperty("email.smtp.socketfactory", "javax.net.SocketFactory");
		Email email = new Email();
		List<Job> jobs = new ArrayList<Job>();
		Job job1 = new Job("C:\\Src\\easybuildMailer\\target\\test-classes\\1469805651252_0.zip",0.0,0.0);
		job1.setTemplateName("HTML5-0001-BB.html");
		job1.setJobId("jobId_12345");
		job1.setJobName("jobName_12345");
		Job job2 = new Job("C:\\Src\\easybuildMailer\\target\\test-classes\\1469805651250_1.zip",0.0,0.0);
		job2.setTemplateName("HTML5-0001-BB.html");
		job2.setJobName("jobName_12345");
		Job job3 = new Job("C:\\Src\\easybuildMailer\\target\\test-classes\\1469805651251_2.zip",0.0,0.0);
		job3.setTemplateName("HTML5-0001-BB.html");
		job3.setJobName("jobName_12345");
		jobs.add(job1);
		jobs.add(job2);
		jobs.add(job3);
		String body = email.buildEmailBody(jobs);

		email.sendEmail(body, "jobId_1234");
	}
}
